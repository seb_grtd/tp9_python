
# ==========================
# La maison qui rend fou
# ==========================

def quel_guichet(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    while not mqrf[guichet] == None:
        guichet = mqrf[guichet]
    return guichet


def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    compteur = 1
    while not mqrf[guichet] == None:
        compteur += 1
        guichet = mqrf[guichet]
    return guichet, compteur


def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    guichets_visites = set()
    # on créé un ensemble des guichets déjà visités pour pouvoir s'assurer que l'on ne tombe pas plusieurs fois sur le même guichet
    # (qu'on tombe dans une boucle infinie)
    compteur = 1
    while not mqrf[guichet] == None:
        if guichet in guichets_visites:
            return None
        guichets_visites.add(guichet)
        guichet = mqrf[guichet]
        compteur +=1
    return guichet, compteur
