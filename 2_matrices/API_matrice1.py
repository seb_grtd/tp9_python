""" Matrices : API n 1 """


def construit_matrice(nb_lignes, nb_colonnes, valeur_par_defaut):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        tuple(int,int,liste): une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    return(nb_lignes, nb_colonnes, [valeur_par_defaut for i in range(nb_lignes*nb_colonnes)])



def set_val(matrice, ligne, colonne, nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case
    """
    if matrice[0] < ligne or matrice[1] <= colonne:
        return 
    matrice[2][matrice[1]*ligne+colonne] = nouvelle_valeur


def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return matrice[0]


def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return matrice[1]


def get_val(matrice, ligne, colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    if get_nb_lignes(matrice) < ligne or get_nb_colonnes(matrice) <= colonne:
        return False
    return matrice[2][matrice[1]*ligne+colonne]

# Fonctions pour l'affichage 

def affiche_ligne_separatrice(matrice, taille_cellule=4):
    """fonction auxilliaire qui permet d'afficher (dans le terminal)
    une ligne séparatrice

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    print()
    for _ in range(get_nb_colonnes(matrice) + 1):
        print('-'*taille_cellule+'+', end='')
    print()


def affiche(matrice, taille_cellule=4):
    """permet d'afficher une matrice dans le terminal

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    nb_colonnes = get_nb_colonnes(matrice)
    nb_lignes = get_nb_lignes(matrice)
    print(' '*taille_cellule+'|', end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule) + '|', end='')
    affiche_ligne_separatrice(matrice, taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule) + '|', end='')
        for j in range(nb_colonnes):
            print(str(get_val(matrice, i, j)).rjust(taille_cellule) + '|', end='')
        affiche_ligne_separatrice(matrice, taille_cellule)
    print()


# Ajouter ici les fonctions supplémentaires, sans oublier de compléter le fichier
# tests_API_matrice.py avec des fonctions de tests

def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str
    """
    fichier = open(nom_fichier, 'r')
    compteur_ligne = 0
    compteur_col = None
    liste_valeurs = []
    for line in fichier:
        compteur_ligne += 1
        valeurs = line.split(',')
        if compteur_col == None:
            compteur_col = len(valeurs)-1
            # -1 pour ne pas compter le '\n' à la fin de la liste 'valeurs'
        for i in range(len(valeurs)-1):
            liste_valeurs.append(valeurs[i])
    return compteur_ligne, compteur_col, liste_valeurs
        
        


def sauve_matrice(matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None
    """
    fichier = open(nom_fichier, 'w')
    for ligne in range(matrice[0]):
        for colonne in range(matrice[1]):
            fichier.write(matrice[2][matrice[1]*ligne+colonne] + ',')
        fichier.write("\n")
    return True


def get_ligne(matrice, ligne):
    """cherche la ligne correspondant à un numéro de ligne

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)
        ligne (int): indice de la ligne que l'on cherche

    Returns:
        list: liste des éléments à la ligne demandée
    """    
    if ligne not in range(matrice[0]):
        return []
    #on test si notre indice de numéro de ligne existe
    #puis on retourne une liste de chaque élément de notre liste de la matrice appartenant à une certaine ligne, on part d'une ligne et on va jusqu'à la fin de celle-ci
    return [matrice[2][i] for i in range(ligne*matrice[1], (ligne+1)*matrice[1])]

def get_colonne(matrice,colonne):
    """cherche la colonne correspondant à un numéro de colonne

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)
        colonne (int): indice de la colonne que l'on cherche

    Returns:
        list: liste des éléments à la colonne demandée
    """       
    if colonne not in range(matrice[1]):
        return []
    #on test si notre indice de numéro de colonne existe
    #puis on retourne une liste de chaque élément de notre liste de la matrice appartenant à une certaine colonne, on part d'une colonne et on va jusqu'à la fin de celle-ci par pas de largeur d'une ligne
    return [matrice[2][i] for i in range(colonne, len(matrice[2]), matrice[1])]

def get_diagonale_principale(matrice):
    """cherche la diagonale principale de notre matrice

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)

    Returns:
        list: liste des éléments de la matrice à sa diagonale principale
    """
    #on retourne tous les éléments de la liste de la matrice en partant du premier jusqu'à la fin par pas de largeur d'une ligne +1    
    return [matrice[2][i] for i in range(0, len(matrice[2]), matrice[1]+1)]

def get_diagonale_secondaire(matrice):
    """cherche la diagonale secondaire de notre matrice

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)

    Returns:
        list: liste des éléments de la matrice à sa diagonale secondaire
    """
    #on retourne tous les éléments de la liste de la matrice en partant de l'indice correspondant à la largeur d'une ligne -1 jusqu'à la fin de la matrice -1, par pas de largeur -1    
    return [matrice[2][i] for i in range(matrice[1]-1, len(matrice[2])-1, matrice[1]-1)]

def transposee(matrice):
    """trouve la transposee d'une matrice

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)

    Returns:
        (tuple(int,int,list)): tuple correspondant à notre représentation de la transposee de notre matrice (hauteur, largeur, liste d'éléments dans l'ordre)
    """    
    nm = (matrice[1],matrice[0],[])
    #on initialise une nouvelle matrice dont les valeurs hauteur/largeur correspondent à celles de la matrice originale, dans l'ordre inverse
    for i in range(matrice[1]):
    #pour chaque i correpsondant à un indice de la colonne de la matrice
        # on ajoute à cette même liste la colonne correspondante à la ligne i (on retourne notre matrice en quelque sorte)
        nm[2].extend(get_colonne(matrice,i))
    return nm
    
def is_triangulaire_inf(matrice):
    """cherche si la matrice est triangulaire infèrieure 

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)

    Returns:
        booleen: retourne si la matrice est triangulaire inférieure ou non
    """    
    for i in range(0,len(matrice[2]),matrice[1]):
    # i allant de 0 jusqu'à la longueur de la liste de la matrice, par pas de la largeur d'une ligne
        for j in range(i, i+matrice[1]):
            # j allant de i jusqu'à i + largeur d'une ligne
            if j-i > i//matrice[1] and matrice[2][j] != None:
                # si j-i(colonne moins le nombre de lignes) > largeur(si le numéro de la colonne est supérieur au numéro de la ligne) et la valeur correspondant à l'indice j de notre matrice est none, celle-ci n'est pas triangulaire
                return False
    return True


def bloc(matrice,ligne,colonne,hauteur,largeur):
    """cherche la sous-matrice de la matrice commençant à la ligne et colonne indiquées 
    et dont les dimensions sont hauteur et largeur.

    Args:
        matrice (tuple(int,int,list)): tuple correspondant à notre représentation d'une matrice (hauteur, largeur, liste d'éléments dans l'ordre)
        ligne (int): indice de la ligne où commencer
        colonne (int): indice de la colonne où commencer
        hauteur (int): hauteur de la sous matrice demandée
        largeur (int): largeur de la sous matrice demandée

    Returns:
        tuple(int,int,liste): retourne la sous matrice de la matrice commençant à la ligne et colonne indiquées
        et dont les dimensions sont hauteur et largeur
    """    
    nouv_mat = []
    if hauteur > matrice[0]:
        hauteur = matrice[0]
    if largeur > matrice[1]:
        largeur = matrice[1]
    if ligne > matrice[0] or colonne > matrice[1]:
        return None
    #plusieurs tests pour voir si les valeurs rentrées sont valides
    for i in range(ligne, ligne+hauteur):
        nouv_mat.extend(get_ligne(matrice,i)[colonne:largeur+colonne])
        #pour chaque ligne en partant de la ligne en jusqu'à la hauteur desirée on rajoute toutes les valeurs de l'indice colonne jsuqu'à l'indice colonne + largeur désirée d'une ligne
    return hauteur, largeur, nouv_mat
