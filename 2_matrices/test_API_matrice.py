""" tests pour les API matrices 
    Remarques : tous les tests de ce fichier doivent passer quelle que soit l'API utilisée
    """
import API_matrice1 as API

def matrice1():
    m1 = API.construit_matrice(3, 4, None)
    API.set_val(m1, 0, 0, 10)
    API.set_val(m1, 0, 1, 11)    
    API.set_val(m1, 0, 2, 12)
    API.set_val(m1, 0, 3, 13)
    API.set_val(m1, 1, 0, 14)
    API.set_val(m1, 1, 1, 15)
    API.set_val(m1, 1, 2, 16)
    API.set_val(m1, 1, 3, 17)
    API.set_val(m1, 2, 0, 18)
    API.set_val(m1, 2, 1, 19)
    API.set_val(m1, 2, 2, 20)
    API.set_val(m1, 2, 3, 21)
    return m1

def matrice2():
    m2 = API.construit_matrice(2, 3, None)
    API.set_val(m2, 0, 0, 'A')
    API.set_val(m2, 0, 1, 'B')    
    API.set_val(m2, 0, 2, 'C')
    API.set_val(m2, 1, 0, 'D')
    API.set_val(m2, 1, 1, 'E')
    API.set_val(m2, 1, 2, 'F')
    return m2

def matrice3():
    m3 = API.construit_matrice(3, 3, None)
    API.set_val(m3, 0, 0, 2)
    API.set_val(m3, 0, 1, 7)    
    API.set_val(m3, 0, 2, 6)
    API.set_val(m3, 1, 0, 9)
    API.set_val(m3, 1, 1, 5)
    API.set_val(m3, 1, 2, 1)
    API.set_val(m3, 2, 0, 4)
    API.set_val(m3, 2, 1, 3)
    API.set_val(m3, 2, 2, 8)
    return m3

def matrice4():
    m4 = API.construit_matrice(4, 4, None)
    API.set_val(m4, 0, 0, 1)
    API.set_val(m4, 1, 0, 2)    
    API.set_val(m4, 1, 1, 3)
    API.set_val(m4, 2, 0, 4)
    API.set_val(m4, 2, 1, 5)
    API.set_val(m4, 2, 2, 6)
    API.set_val(m4, 3, 0, 7)
    API.set_val(m4, 3, 1, 8)
    API.set_val(m4, 3, 2, 9)
    API.set_val(m4, 3, 3, 10)
    return m4

def test_construit_matrice():
    assert API.construit_matrice(3,4,None) == (3,4, [None, None, None, None, None, None, None, None, None, None, None, None])
    assert API.construit_matrice(2,3,None) == (2,3, [None, None, None, None, None, None])
    assert API.construit_matrice(3,3,None) == (3,3, [None, None, None, None, None, None, None, None, None])

def test_get_nb_lignes():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_nb_lignes(m1) == 3
    assert API.get_nb_lignes(m2) == 2
    assert API.get_nb_lignes(m3) == 3
        
def test_get_nb_colonnes():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_nb_colonnes(m1) == 4
    assert API.get_nb_colonnes(m2) == 3
    assert API.get_nb_colonnes(m3) == 3

def test_get_val():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_val(m1, 0, 1) == 11
    assert API.get_val(m1, 2, 1) == 19
    assert API.get_val(m2, 1, 1) == 'E'
    assert API.get_val(m2, 0, 2) == 'C'
    assert API.get_val(m3, 2, 0) == 4
    assert API.get_val(m3, 1, 0) == 9

def test_sauve_charge_matrice():
    matrice = matrice2()
    API.sauve_matrice(matrice, "matrice1.csv")
    matrice_bis = API.charge_matrice_str("matrice1.csv")
    assert matrice == matrice_bis
    
def test_get_ligne():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_ligne(m1,0) == [10,11,12,13]
    assert API.get_ligne(m1,3) == []
    assert API.get_ligne(m1,1) == [14,15,16,17]
    assert API.get_ligne(m1,2) == [18,19,20,21]
    assert API.get_ligne(m2,0) == ["A","B","C"]
    assert API.get_ligne(m2,1) == ["D","E","F"]
    assert API.get_ligne(m2,-1) == []
    assert API.get_ligne(m3,0) == [2,7,6]
    assert API.get_ligne(m3,1) == [9,5,1]
    assert API.get_ligne(m3,2) == [4,3,8]
    
def test_get_colonne():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_colonne(m1,0) == [10,14,18]
    assert API.get_colonne(m1,4) == []
    assert API.get_colonne(m1,1) == [11,15,19]
    assert API.get_colonne(m1,2) == [12,16,20]
    assert API.get_colonne(m1,3) == [13,17,21]
    assert API.get_colonne(m2,0) == ["A","D"]
    assert API.get_colonne(m2,1) == ["B","E"]
    assert API.get_colonne(m2,2) == ["C","F"]
    assert API.get_colonne(m2,-1) == []
    assert API.get_colonne(m3,0) == [2,9,4]
    assert API.get_colonne(m3,1) == [7,5,3]
    assert API.get_colonne(m3,2) == [6,1,8]
    
def test_get_diagonale_principale():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_diagonale_principale(m1) == [10,15,20]
    assert API.get_diagonale_principale(m2) == ['A','E']
    assert API.get_diagonale_principale(m3) == [2,5,8]
    
def test_get_diagonale_secondaire():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_diagonale_secondaire(m1) == [13,16,19]
    assert API.get_diagonale_secondaire(m2) == ['C','E']
    assert API.get_diagonale_secondaire(m3) == [6,5,4]
    
def test_transposee():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.transposee(m1) == (m1[1],m1[0], [10,14,18,11,15,19,12,16,20,13,17,21])
    assert API.transposee(m2) == (m2[1],m2[0], ["A","D","B","E","C","F"])
    assert API.transposee(m3) == (m3[1],m3[0], [2,9,4,7,5,3,6,1,8])
    
def test_is_triangulaire_inf():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    m4 = matrice4()
    assert API.is_triangulaire_inf(m1) == False
    assert API.is_triangulaire_inf(m2) == False
    assert API.is_triangulaire_inf(m3) == False
    assert API.is_triangulaire_inf(m4) == True

def test_bloc():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.bloc(m1,1,2,2,2) == (2,2, [16,17,20,21])
    assert API.bloc(m1,1,2,2,1) == (2,1, [16,20])
    assert API.bloc(m2,0,0,2,3) == m2
    assert API.bloc(m2,0,2,2,1) == (2,1, ['C','F'])
    assert API.bloc(m2,0,1,2,2) == (2,2, ['B','C', 'E', 'F'])
    assert API.bloc(m3,1,1,0,0) == (0,0, [])
    assert API.bloc(m3,1,1,0,0) == (0,0, [])
    assert API.bloc(m3,1,0,2,3) == (2,3, [9,5,1,4,3,8])
    assert API.bloc(m1,0,0,6,6) == m1
    assert API.bloc(m1,6,0,3,2) == None
    assert API.bloc(m1,0,6,3,2) == None
    
API.affiche(matrice1())
print(matrice1())
API.affiche(matrice2())
print(matrice2())
API.affiche(matrice3())
print(matrice3())
API.affiche(matrice4())
print(matrice4())