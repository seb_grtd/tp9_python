"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    #Complexité O(N)
    familles = set()
    for pokemon in pokedex:
        familles.add(pokemon[1])
    return familles

def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    #complexité O(n)
    compteur = 0
    for pokemon in pokedex:
        if pokemon[1] == famille:
            compteur +=1
    return compteur

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    #Complexité O(n)
    frequence_famille = {}
    for pokemon in pokedex:
        if pokemon[1] not in frequence_famille.keys():
            frequence_famille[pokemon[1]] = 1
        else:
            frequence_famille[pokemon[1]] +=1
    return frequence_famille

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    #Complexité O(n)
    dico_pokemon_dans_familles = {}
    for pokemon in pokedex:
        if pokemon[1] not in dico_pokemon_dans_familles.keys():
            dico_pokemon_dans_familles[pokemon[1]] = {pokemon[0]}
        else:
            dico_pokemon_dans_familles[pokemon[1]].add(pokemon[0])
    return dico_pokemon_dans_familles

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    #Complexité O(N) + O(N) => O(N)
    famille_max = None
    frequence_max = None
    for famille, frequence in frequences_famille(pokedex).items():
        if frequence_max == None or frequence_max < frequence:
            frequence_max = frequence
            famille_max = famille
    return famille_max


# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    familles = set()
    for familles_du_pokemon in pokedex.values():
        for famille in familles_du_pokemon:
            familles.add(famille)
    return familles



def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    compteur= 0
    for familles_du_pokemon in pokedex.values():
        if famille in familles_du_pokemon:
            compteur += 1
    return compteur

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    frequence_famille_pokedex = {}
    for familles_du_pokemon in pokedex.values():
        for famille in familles_du_pokemon:
            frequence_famille_pokedex[famille] = nombre_pokemons_v2(pokedex, famille)
    return frequence_famille_pokedex

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_familles_pokemon = {}
    for pokemon, familles_du_pokemon in pokedex.items():
        for famille in familles_du_pokemon:
            if famille not in dico_familles_pokemon.keys():
                dico_familles_pokemon[famille] = {pokemon}
            else:
                dico_familles_pokemon[famille].add(pokemon)
    return dico_familles_pokemon

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    famille_max = None
    frequence_max = None
    for famille, frequence in frequences_famille_v2(pokedex).items():
        if  famille_max == None or frequence > frequence_max:
            frequence_max = frequence
            famille_max = famille
    return famille_max
